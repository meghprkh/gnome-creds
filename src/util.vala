private class Util {
    public static Type key_or_password(Secret.Item item) {
        if (!item.attributes.contains("xdg:schema")) return typeof(Key);

        string schema = item.attributes["xdg:schema"];
        if (schema == "chrome_libsecret_password_schema") return typeof(Password);
        if (schema == "org.gnome.keyring.NetworkPassword") return typeof(Password);
        if (schema == "org.epiphany.FormPassword") return typeof(Password);

        // XXX: Debug print
        print("\n%s - %s\n", item.label, item.get_secret().get_text());
        item.attributes.foreach((key, val) => {
            print("\t%s - %s\n", key, val);
        });

        return typeof(Key);
    }
}
