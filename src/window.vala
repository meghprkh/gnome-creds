/* window.vala
 *
 * Copyright 2018 Megh Parikh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace GnomeCreds {
	[GtkTemplate (ui = "/org/gnome/Gnome-Creds/ui/window.ui")]
	public class Window : Gtk.ApplicationWindow {
		[GtkChild]
		Gtk.ListBox list;
		private ListStore passwords;

		public Window (Gtk.Application app) {
			Object (application: app);
			passwords = new ListStore(typeof(Secret.Item));
			list.bind_model(passwords, (item) => {
			    return new ListViewRow(item);
			});
			temp();
		}
		
		public void temp() {
		    Secret.Service service = Secret.Service.get_sync(Secret.ServiceFlags.LOAD_COLLECTIONS);
	        List<Secret.Collection?> collections = service.get_collections();

            foreach (Secret.Collection collection in collections) {
                print("\nKeyring - %s:\n", collection.label);
                
                collection.load_items_sync();
                List<Secret.Item> items = collection.get_items();

                foreach (Secret.Item item in items) {
                    item.load_secret_sync();

                    if (Util.key_or_password(item) == typeof(Password)) passwords.append(item);
                }
            }
		}
	}
}
