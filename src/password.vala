private class Password {
    public string site;
    public string label;
    public string email;
    public string password;
    public DateTime last_used;

    public Password(Secret.Item item) {
        password = item.get_secret().get_text();
        var schema = item.attributes["xdg:schema"];

        if (schema == "chrome_libsecret_password_schema") {
            site = item.attributes["signon_realm"];
            label = site;
            email = item.attributes["username_value"];
            // XXX: No better field?
            var timestamp = int64.parse(item.attributes["date_synced"][0:-7]);
            last_used = new DateTime.from_unix_local(timestamp);
        } else if (schema == "org.gnome.keyring.NetworkPassword") {
            site = item.attributes["server"];
            label = site;
            email = item.attributes["user"];
        } else if (schema == "org.epiphany.FormPassword") {
            site = item.attributes["target_origin"];
            label = site;
            email = item.attributes["username"];
        } else {
            label = item.label;
        }
    }
}
