/* window.vala
 *
 * Copyright 2018 Megh Parikh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace GnomeCreds {
	[GtkTemplate (ui = "/org/gnome/Gnome-Creds/ui/list-view-row.ui")]
	public class ListViewRow: Gtk.Box {
		[GtkChild]
		Gtk.Label site_label;

		[GtkChild]
		Gtk.Label email_label;

		[GtkChild]
		Gtk.Label last_used_label;

        private Password password;

		public ListViewRow (Object item) {
		    this.password = new Password((Secret.Item) item);
            site_label.label = password.site;
            email_label.label = password.email;
            last_used_label.label = password.last_used.to_string();
		}
	}
}
